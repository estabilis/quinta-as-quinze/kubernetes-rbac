resource "kubernetes_namespace_v1" "main" {
  metadata {
    name = "${local.prefix}-ns"
  }
}


# Demo com Python
resource "kubernetes_secret_v1" "cron" {
  metadata {
    name      = "${local.prefix}-secret-sa-cron"
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }
}

resource "kubernetes_service_account_v1" "cron" {
  metadata {
    name      = "${local.prefix}-sa-cron"
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }
  secret {
    name = kubernetes_secret_v1.cron.metadata[0].name
  }
}

resource "kubernetes_cluster_role" "cron" {
  metadata {
    name = "${local.prefix}-cr"
  }

  rule {
    api_groups = [""]
    resources  = ["namespaces"]
    verbs      = ["create", "list", "delete"]
  }
}

resource "kubernetes_cluster_role_binding_v1" "cron" {
  metadata {
    name = "${local.prefix}-crb"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.cron.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.cron.metadata[0].name
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }
}

resource "kubernetes_config_map_v1" "cron" {
  metadata {
    name      = "${local.prefix}-cmap"
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }

  data = {
    "main.py" = "${file("${path.module}/main.py")}"
  }
}

resource "kubernetes_cron_job_v1" "cron" {
  metadata {
    name      = "${local.prefix}-cjob"
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }
  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 5
    schedule                      = "* * * * *"
    timezone                      = "Etc/UTC"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10
    job_template {
      metadata {}
      spec {
        backoff_limit = 3
        template {
          metadata {}
          spec {
            service_account_name = kubernetes_service_account_v1.cron.metadata[0].name
            volume {
              name = "${local.prefix}-volume"
              config_map {
                name = kubernetes_config_map_v1.cron.metadata[0].name
              }
            }
            container {
              name    = "${local.prefix}-container"
              image   = "python:slim-buster"
              command = ["sh", "-c", "pip install kubernetes && python /app/main.py"]
              volume_mount {
                name       = "${local.prefix}-volume"
                mount_path = "/app"
              }
            }
          }
        }
      }
    }
  }
}


# Demo com kubectl 
resource "kubernetes_secret_v1" "ctl" {
  metadata {
    name      = "${local.prefix}-secret-sa-ctl"
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }
}

resource "kubernetes_service_account_v1" "ctl" {
  metadata {
    name      = "${local.prefix}-sa-ctl"
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }
  secret {
    name = kubernetes_secret_v1.ctl.metadata[0].name
  }
}

resource "kubernetes_role_v1" "ctl" {
  metadata {
    name      = "${local.prefix}-role"
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }

  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "create"]
  }
}

resource "kubernetes_role_binding_v1" "ctl" {
  metadata {
    name      = "${local.prefix}-rb"
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role_v1.ctl.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.ctl.metadata[0].name
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }
}

resource "kubernetes_pod_v1" "ctl" {
  metadata {
    name      = "${local.prefix}-ctl"
    namespace = kubernetes_namespace_v1.main.metadata[0].name
  }

  spec {
    service_account_name = kubernetes_service_account_v1.ctl.metadata[0].name
    container {
      image   = "bitnami/kubectl"
      name    = "${local.prefix}-container"
      command = ["sh", "-c", "echo 'Hello, Kubernetes!' && sleep 3600"]
    }
  }
}