resource "aws_eks_cluster" "main" {
  name     = "${local.prefix}-lab"
  role_arn = aws_iam_role.eks_cluster.arn
  version  = 1.26

  vpc_config {
    subnet_ids              = toset(aws_subnet.public.*.id)
    endpoint_private_access = false
    endpoint_public_access  = true
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.AmazonEKSVPCResourceController,
  ]
}

resource "aws_eks_node_group" "main" {
  cluster_name           = aws_eks_cluster.main.name
  node_group_name_prefix = local.prefix
  node_role_arn          = aws_iam_role.eks_nodes.arn
  capacity_type          = "SPOT"
  subnet_ids             = toset(aws_subnet.private.*.id)
  instance_types         = ["t3.medium"]

  scaling_config {
    desired_size = 2
    max_size     = 3
    min_size     = 1
  }

  update_config {
    max_unavailable = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]
}